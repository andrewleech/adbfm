package main

import "github.com/gdamore/tcell"

func selectItem(ev *tcell.EventKey) *tcell.EventKey {
	if currentFocus == 0 {
		return ev
	}

	return nil

}

func previousItem(ev *tcell.EventKey) *tcell.EventKey {
	if currentFocus == 0 {
		return ev
	}

	return nil

}

func nextItem(ev *tcell.EventKey) *tcell.EventKey {
	if currentFocus == 0 {
		return ev
	}

	return nil

}

func previousPage(ev *tcell.EventKey) *tcell.EventKey {
	if currentFocus == 0 {
		return ev
	}

	return nil

}

func nextPage(ev *tcell.EventKey) *tcell.EventKey {
	if currentFocus == 0 {
		return ev
	}

	return nil

}

func previousField(_ *tcell.EventKey) *tcell.EventKey {
	if currentFocus > 0 {
		currentFocus--
	}

	focusUpdated()

	return nil
}

func nextField(_ *tcell.EventKey) *tcell.EventKey {
	if currentFocus < 2 {
		currentFocus++
	}

	focusUpdated()

	return nil
}

func exit(_ *tcell.EventKey) *tcell.EventKey {
	return nil
}
