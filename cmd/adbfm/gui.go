package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/gdamore/tcell"
	"gitlab.com/tslocum/cbind"
	"gitlab.com/tslocum/cview"
)

var (
	app *cview.Application

	inputConfig = cbind.NewConfiguration()

	devicesDropDown *cview.DropDown
	localBuffer     *cview.TextView
	remoteBuffer    *cview.TextView

	currentFocus = 0
)

func initTUI() error {
	devices, err := adb.ListDevices()
	if err != nil {
		log.Fatalf("failed to list devices: %s", err)
	}

	if len(devices) == 0 {
		log.Fatal("no devices")
	}

	cview.Styles.TitleColor = tcell.ColorDefault
	cview.Styles.BorderColor = tcell.ColorDefault
	cview.Styles.PrimaryTextColor = tcell.ColorDefault
	cview.Styles.PrimitiveBackgroundColor = tcell.ColorDefault

	app = cview.NewApplication().
		SetInputCapture(inputConfig.Capture)

	devicesDropDown = cview.NewDropDown().SetLabel(" Device: ").SetLabelColor(tcell.ColorDefault)
	for _, device := range devices {
		devicesDropDown.AddOption(device.Serial, setDeviceFunc(adb, device))
	}

	localBuffer = cview.NewTextView().SetWrap(true).SetWordWrap(false)
	localBuffer.SetTitleAlign(cview.AlignLeft).SetBorder(true)

	remoteBuffer = cview.NewTextView().SetWrap(true).SetWordWrap(false)
	remoteBuffer.SetTitleAlign(cview.AlignLeft).SetBorder(true)

	// Select first device
	if len(devices) > 0 {
		devicesDropDown.SetCurrentOption(0)
	}

	pad := cview.NewTextView()

	grid := cview.NewGrid().
		SetRows(1, 1, -1).
		SetColumns(-1, -1).
		AddItem(devicesDropDown, 0, 0, 1, 2, 0, 0, true).
		AddItem(pad, 1, 0, 1, 2, 0, 0, true).
		AddItem(localBuffer, 2, 0, 1, 1, 0, 0, false).
		AddItem(remoteBuffer, 2, 1, 1, 1, 0, 0, false)

	app.SetRoot(grid, true).SetFocus(devicesDropDown)

	// TODO No errors possible?
	return nil
}

func focusUpdated() {
	switch currentFocus {
	case 0:
		app.SetFocus(devicesDropDown)
	case 1:
		app.SetFocus(localBuffer)
	case 2:
		app.SetFocus(remoteBuffer)
	}
}

func listLocalDir(p string) {
	var entries []string
	err := filepath.Walk(p, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		if info.IsDir() {
			return nil
		}

		entries = append(entries, info.Name())
		return nil
	})
	if err != nil {
		log.Fatal(err)
	}

	localBuffer.SetTitle(" " + p + " ")

	localBuffer.Clear()
	for _, entry := range entries {
		fmt.Fprintf(localBuffer, entry+"\n")
	}
}
