package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/gdamore/tcell"
	goadb "github.com/zach-klippenstein/goadb"
	"gitlab.com/tslocum/adbfm/pkg/android"
	"gitlab.com/tslocum/cbind"
)

type appConfig struct {
	Input map[string][]string // Keybinds
}

var config = &appConfig{}

var (
	adb *android.ADB

	done = make(chan bool)
)

const (
	actionSelect        = "select"
	actionPreviousItem  = "previous-item"
	actionNextItem      = "next-item"
	actionPreviousPage  = "previous-page"
	actionNextPage      = "next-page"
	actionPreviousField = "previous-field"
	actionNextField     = "next-field"
	actionExit          = "exit"
)

var actionHandlers = map[string]func(*tcell.EventKey) *tcell.EventKey{
	actionSelect:        selectItem,
	actionPreviousItem:  previousItem,
	actionNextItem:      nextItem,
	actionPreviousPage:  previousPage,
	actionNextPage:      nextPage,
	actionPreviousField: previousField,
	actionNextField:     nextField,
	actionExit:          exit,
}

func setKeyBinds() error {
	if len(config.Input) == 0 {
		setDefaultKeyBinds()
	}

	for a, keys := range config.Input {
		a = strings.ToLower(a)
		handler := actionHandlers[a]
		if handler == nil {
			return fmt.Errorf("failed to set keybind for %s: unknown action", a)
		}

		for _, k := range keys {
			mod, key, ch, err := cbind.Decode(k)
			if err != nil {
				return fmt.Errorf("failed to set keybind %s for %s: %s", k, a, err)
			}

			if key == tcell.KeyRune {
				inputConfig.SetRune(mod, ch, handler)
			} else {
				inputConfig.SetKey(mod, key, handler)
			}
		}
	}

	return nil
}

func setDefaultKeyBinds() {
	config.Input = map[string][]string{
		actionSelect:        {"Enter"},
		actionPreviousItem:  {"Up", "k"},
		actionNextItem:      {"Down", "j"},
		actionPreviousPage:  {"PageUp"},
		actionNextPage:      {"PageDown"},
		actionPreviousField: {"Backtab"},
		actionNextField:     {"Tab"},
		actionExit:          {"Alt+q"},
	}
}

func setDeviceFunc(adb *android.ADB, device *goadb.DeviceInfo) func() {
	return func() {
		err := adb.SetDevice(device)
		if err != nil {
			log.Fatal(err)
		}

		remoteBuffer.Clear()

		p := "/storage/emulated/0"

		remoteBuffer.SetTitle(" " + p + " ")

		entries, err := adb.DirectoryEntries(p)
		if err != nil {
			log.Fatalf("failed to list files: %s", err)
		}

		for _, entry := range entries {
			fmt.Fprintf(remoteBuffer, entry.Name+"\n")
		}
	}
}

func main() {
	var err error
	adb, err = android.NewADB("", 0, "")
	if err != nil {
		log.Fatalf("failed to connect to ADB server: %s", err)
	}

	err = setKeyBinds()
	if err != nil {
		log.Fatalf("failed to set keybinds: %s", err)
	}

	err = initTUI()
	if err != nil {
		log.Fatalf("failed to initialize user interface: %s", err)
	}

	listLocalDir("/home/trevor/Downloads")

	go func() {
		if err := app.Run(); err != nil {
			log.Fatalf("failed to run application: %s", err)
		}

		done <- true
	}()

	<-done
}
