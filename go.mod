module gitlab.com/tslocum/adbfm

go 1.13

require (
	github.com/gdamore/tcell v1.3.0
	github.com/zach-klippenstein/goadb v0.0.0-20170530005145-029cc6bee481
	gitlab.com/tslocum/cbind v0.1.0
	gitlab.com/tslocum/cview v1.4.2-0.20200128151041-339db80f666d
	golang.org/x/sys v0.0.0-20200124204421-9fbb57f87de9 // indirect
)
