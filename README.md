# adbfm
[![GoDoc](https://godoc.org/gitlab.com/tslocum/adbfm?status.svg)](https://godoc.org/gitlab.com/tslocum/adbfm)
[![CI status](https://gitlab.com/tslocum/adbfm/badges/master/pipeline.svg)](https://gitlab.com/tslocum/adbfm/commits/master)
[![Donate](https://img.shields.io/liberapay/receives/rocketnine.space.svg?logo=liberapay)](https://liberapay.com/rocketnine.space)

ADB file manager

## Dependencies

* [zach-klippenstein/goadb](https://github.com/zach-klippenstein/goadb)

## Documentation

Documentation is available via [gdooc](https://godoc.org/gitlab.com/tslocum/adbfm).

## Support

Please share issues/suggestions [here](https://gitlab.com/tslocum/adbfm/issues).
