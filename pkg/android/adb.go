package android

import (
	"errors"
	"fmt"
	"log"
	"sync"

	adb "github.com/zach-klippenstein/goadb"
)

// DefaultPort is the default port the ADB server listens on.
const DefaultPort = 5037

// ADB represents a connection with an ADB server.
type ADB struct {
	bridge *adb.Adb
	device *adb.Device

	*sync.Mutex
}

// NewADB establishes a connection with an ADB server. When host and port are
// unspecified they are replaced with localhost and the default port.
func NewADB(host string, port int, pathToADB string) (*ADB, error) {
	bridge, err := adb.NewWithConfig(adb.ServerConfig{
		PathToAdb: pathToADB,
		Host:      host,
		Port:      port,
	})
	if err != nil {
		log.Fatal(err)
	}

	bridge.StartServer() // Ignore error

	_, err = bridge.ServerVersion()
	if err != nil {
		return nil, fmt.Errorf("failed to get server version: %s", err)
	}

	a := ADB{bridge: bridge, Mutex: new(sync.Mutex)}
	return &a, nil
}

func (a *ADB) ListDevices() (devices []*adb.DeviceInfo, err error) {
	a.Lock()
	defer a.Unlock()

	deviceInfoList, err := a.bridge.ListDevices()
	if err != nil {
		return nil, err
	}

	for _, deviceInfo := range deviceInfoList {
		devices = append(devices, deviceInfo)
	}

	return devices, nil
}

func (a *ADB) SetDevice(deviceInfo *adb.DeviceInfo) (err error) {
	a.Lock()
	defer a.Unlock()

	return a.setDevice(deviceInfo)
}

func (a *ADB) setDevice(deviceInfo *adb.DeviceInfo) (err error) {
	device := a.bridge.Device(adb.DeviceWithSerial(deviceInfo.Serial))
	if device == nil {
		return fmt.Errorf("failed to get device with serial %s", deviceInfo.Serial)
	}

	a.device = device
	return nil
}

func (a *ADB) DirectoryEntries(path string) (entries []*adb.DirEntry, err error) {
	a.Lock()
	defer a.Unlock()

	if a.device == nil {
		return nil, errors.New("no device")
	}

	listEntries, err := a.device.ListDirEntries(path)
	if err != nil {
		return nil, fmt.Errorf("failed to get dirents: %s", err)
	}

	for listEntries.Next() {
		entries = append(entries, listEntries.Entry())
	}
	if listEntries.Err() != nil {
		return nil, fmt.Errorf("failed to list files: %s", err)
	}

	return entries, nil
}
